（v1.8.1）2022 年 09 月 14 日
1. 验证码发送支持传租户号
2. 修复已知问题

（v1.8.0）2022 年 06 月 21 日
1. 新增标准的客户端接口服务提供者
2. 优化接口响应数据类的设计

（v1.7.1）2021 年 09 月 13 日
1. 修复 Response 类数据构建时的兼容性及异常逻辑问题

（v1.7.0）2021 年 09 月 01 日
1. 兼容性重构通行证服务端接口服务调用的设计
2. 新增服务端接口单元测试用例
3. 更新 mingyuanyun/ggcp-http 的版本依赖

2021 年 08 月 17 日
1. 修复已知问题

2021 年 04 月 29 日
1. 修复一些已知问题

2021 年 03 月 26 日
1. 调整一些已知问题

2021 年 02 月 04 日
1. 引入 mingyuanyun\ggcp-http 扩展的依赖
2. 重写底层所有接口调用逻辑

2021 年 02 月 04 日
1. 调整用户授权认证（审批申请）状态相关接口传参
2. 新增根据 UnionID 获取用户列表的方法

2020 年 07 月 16 日
1. 修复单点登录接口调用的问题

2020 年 07 月 15 日
1. 新增大量通行证对外提供支持的接口调用封装
2. 提供 getApiResponseFaildMsg 方法用于获取内部接口调用失败时的错误描述信息
3. 修复 components 使用模式下 SDK 组件配置信息异常的问题
4. 优化 components 使用模式下对 IDE 的解析支持

2020 年 06 月 10 日
1. 修复令牌校验的缓存会带来判断不正确的问题
2. 通行证用户数据增加 isVerified（手机号是否验证） 的属性

2020 年 06 月 03 日
1. 优化初始化通行证用户的处理，支持对外返回初始化失败的用户数据

2020 年 05 月 18 日
1. 调整通行证接口请求失败时记录的日志等级

2020 年 04 月 10 日
1. 修复令牌刷新方法异常逻辑
2. 调整变更认证状态方法传参
3. 更新相关测试方法、注释、说明文档

2020 年 03 月 31 日
1. 新增变更通行证用户认证状态方法
2. 新增向通行证初始化用户数据方法
3. 优化部分注释内容

2020 年 03 月 21 日
1. 基于 Composer 发布第一版明源云链统一通行证客户端 SDK