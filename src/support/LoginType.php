<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support;

use iPass\support\traits\ConstantTrait;

class LoginType
{
    use ConstantTrait;
    
    /** 手机号+短信验证码登录 */
    const TYPE_MOBILE_AND_SMS = 'MOBILE_AND_SMS';
    /** 账号/手机号+密码登录 */
    const TYPE_ACCOUNT_AND_PWD = 'ACCOUNT_AND_PWD';
}