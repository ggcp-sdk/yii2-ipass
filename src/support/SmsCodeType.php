<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support;

use iPass\support\traits\ConstantTrait;

class SmsCodeType
{
    use ConstantTrait;
    
    /** 注册验证码类型 */
    const TYPE_REGISTER = 'REGISTER_SMS';
    /** 登录验证码类型 */
    const TYPE_LOGIN = 'LOGIN_SMS';
    /** 修改密码验证码类型 */
    const TYPE_CHANGE_PWD = 'CHANGE_PWD_SMS';
    /** 绑定手机号验证码类型 */
    const TYPE_BIND_MOBILE = 'BIND_MOBILE_SMS';
}