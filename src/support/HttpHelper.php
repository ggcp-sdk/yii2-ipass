<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support;

use iPass\Config;
use iPass\HttpException;

class HttpHelper
{
    /**
     * 请求头
     *
     * @var array
     */
    protected static $httpHeaders = [];

    /**
     * 接口业务处理失败的响应说明内容队列
     * 
     * @var string[]
     */
    protected static $httpBizFaildMsgQueue = [];

    /**
     * Http GET 请求方式调用接口
     *
     * @param  string $url      接口地址
     * @param  array  $params   Query 参数数组
     * @param  int    $timeout  接口超时时间设置（毫秒），默认 3s 超时
     * @return mixed|false 请求成功时返回响应内容，失败时返回 false。可以通过 getHttpErrMsg()
     * 方法获取对应的错误内容
     */
    public static function get($url, $params = [], $timeout = 3000)
    {
        $ch = curl_init();

        $url = self::buildQueryParams($url, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 1000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        if (!empty(self::$httpHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, self::$httpHeaders);
        }

        $res = curl_exec($ch);
        return self::response($res, $ch);
    }

    /**
     * Http POST 请求方式调用接口
     *
     * @param  string $url      接口地址
     * @param  array  $post     POST 请求数据数组
     * @param  int    $timeout  接口超时时间设置（毫秒），默认 3s 超时
     * @param  bool   $isJson   是否以 Json 格式的数据进行接口请求，默认 true
     * @return mixed|false 请求成功时返回响应内容，失败时返回 false。可以通过 getHttpErrMsg()
     * 方法获取对应的错误内容
     */
    public static function post($url, $post, $timeout = 3000, $isJson = true)
    {
        $ch = curl_init();

        if ($isJson) {
            $post = json_encode($post);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);
            self::$httpHeaders[] = 'Content-Type: application/json; charset=utf-8';
        } else {
            $post = http_build_query($post);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 500);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        if (!empty(self::$httpHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, self::$httpHeaders);
        }

        $res = curl_exec($ch);
        return self::response($res, $ch);
    }

    /**
     * 构建 URL Query Params
     *
     * @param  string $url      接口地址
     * @param  array  $params   需要拼接的地址栏参数
     * @return string
     */
    public static function buildQueryParams($url, $params)
    {
        $params = http_build_query($params);
        return $url . (strpos($url, '?') ? '&' : '?') . $params;
    }

    /**
     * @param  string $bizFaildMsg 接口调用失败的业务说明内容
     */
    public static function pushHttpBizFaildMsg($bizFaildMsg)
    {
        self::$httpBizFaildMsgQueue[] = $bizFaildMsg;
    }

    /**
     * @return string
     */
    public static function popHttpBizFaildMsg()
    {
        return array_pop(self::$httpBizFaildMsgQueue);
    }

    /**
     * 设置接口请求所需要使用的请求头数据
     *
     * @param  array $headers   请求头数据：["Content-Type" => "application/json"]
     * @return $this
     */
    protected function setHttpHeaders(array $headers)
    {
        foreach ($headers as $key => $value) {
            $this->httpHeaders[] = sprintf('%s: %s', $key, $value);
        }
        return $this;
    }

    /**
     * 将接口放回的数据简单处理后返回给调用方
     *
     * @param  string   $apiRes 接口响应的数据内容
     * @param  resource $ch     CURL 资源句柄
     * @return mixed
     */
    protected static function response($apiRes, $ch)
    {
        if ($apiRes === false) {
            \Yii::error('通行证接口异常: ' . curl_error($ch));
            // $errmsg = curl_error($ch);
            curl_close($ch);
            // throw new HttpException("Exception api response: {$errmsg}.");
            return false;
        }

        $apiResHttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($apiResHttpCode != 200) {
            curl_close($ch);
            return false;
        }

        $responseType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $isJson       = strpos(strtolower($responseType), 'json') !== false;
        // 兼容不规范的接口返回
        if (!$isJson) {
            // 有些人的接口就是乱来，明明返回的是 json 格式，Content-Type 非要给成 text/html
            if ('{' === substr($apiRes, 0, 1) && substr($apiRes, -1) === '}') {
                $isJson = true;
            }
        }

        $apiRes = $isJson ? json_decode($apiRes, true) : $apiRes;

        curl_close($ch);
        
        // 如果接口调用返回了业务执行错误内容，将内容栈存起来，方便外部获取
        $apiRes['errcode'] !== Config::API_SUCCESS && self::pushHttpBizFaildMsg($apiRes['message']);
        return $apiRes;
    }
}
