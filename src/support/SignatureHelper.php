<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support;

class SignatureHelper
{
    /**
     * 参数签名
     *
     * @param  array $params        参数数组
     * @param  array $signFields    参与签名的参数名
     * @return string
     */
    public static function signature($params, $signFields = [])
    {
        if (!empty($signFields)) {
            foreach ($params as $f => $v) {
                if (!in_array($f, $signFields)) {
                    unset($params[$f]);
                }
            }
        }

        ksort($params);

        $beforeSignStr = '';
        foreach ($params as $k => $v) {
            $beforeSignStr .= sprintf('%s=%s&', $k, $v);
        }

        return sha1(trim($beforeSignStr, '&'));
    }

    /**
     * 生成一个 Nonce 随机字符串
     *
     * @return string
     */
    public static function generateNonce($len = 32)
    {
        $nonce      = '';
        $charStr    = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $charStrLen = strlen($charStr) - 1;
        for ($i = 0; $i < intval($len); $i++) {
            $nonce .= $charStr[rand(0, $charStrLen)];
        }
        return $nonce;
    }
}