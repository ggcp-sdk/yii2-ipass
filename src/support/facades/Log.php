<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support\facades;

use iPass\support\Facade;
use iPass\support\YiiLogger;

/**
 * @method static void info(string $message, string $category = 'application')
 * @method static void warning(string $message, string $category = 'application')
 * @method static void error(string $message, string $category = 'application')
 */
class Log extends Facade
{
    protected static function getFacadeAccessor()
    {
        return YiiLogger::class;
    }
}