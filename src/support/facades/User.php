<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support\facades;

use iPass\providers\UserProvider;
use iPass\support\Facade;

/**
 * @method static \iPass\struct\User|null load(string $accessToken)
 * 根据通行证中心颁发的访问令牌获取对应的通行证用户身份
 *
 * @method static void changeApproval(string $accessToken, string $openId, int $status)
 * 修改通行证用户在租户下的授权认证状态
 *
 * @method static array initUsers(array $initUsers, string $tenantCode)
 * 向统一通行证初始化用户数据，初始化成功后，将会带上用户对应的通行证
 * UnionID 数据返回。用户数组 $unifiedPassUsers 中每个用户需要提
 * 供的数据必须要有 [mobile|account|email] 三者其一，除此之外
 * nickname、realname、gender、avatarUrl、password 等参数选填。
 * 1. 选填属性如果通行证中对应用户的属性内容不为空，则不会被更新，以
 * 通行证的数据为准。
 * 2. 需要注意的是，通过该方法同步过的通行证用户，无论是新增还是已存
 * 在的用户都会被修改为认证通过的状态
 */
class User extends Facade
{
    /**
     * 获取注册的通行证用户门面处理器
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return UserProvider::class;
    }
}
