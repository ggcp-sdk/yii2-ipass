<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\support\facades;

use iPass\providers\TokenProvider;
use iPass\support\Facade;

/**
 * @method static bool validate(string $accessToken)
 * 验证通行证中心颁发的访问令牌目前是否有效
 * 
 * @method static \iPass\providers\TokenProvider|null refresh(string $refreshToken)
 * 利用通行证中心颁发的刷新令牌获取（新的）有效授权令牌
 */
class Token extends Facade
{
    /**
     * 获取注册的 Token 门面处理器
     * 
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return TokenProvider::class;
    }
}