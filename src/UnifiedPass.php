<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass;

use iPass\client\ClientProvider;
use iPass\providers\ManageProvider;
use iPass\providers\SmsProvider;
use iPass\providers\SsoProvider;
use iPass\providers\TenantProvider;
use iPass\providers\TokenProvider;
use iPass\providers\UserProvider;
use iPass\server\ServerProvider;
use iPass\struct\Token;
use iPass\struct\User;
use iPass\support\HttpHelper;
use iPass\support\traits\SingletonTrait;

class UnifiedPass extends \yii\base\Component
{
    use SingletonTrait;

    /**
     * 通行证用户服务提供者实例
     *
     * @var \iPass\providers\UserProvider
     * @deprecated
     */
    public $userProvider;

    /**
     * 通行证令牌服务提供者实例
     *
     * @var \iPass\providers\TokenProvider
     * @deprecated
     */
    public $tokenProvider;

    /**
     * 通行证短信服务提供者实例
     *
     * @var \iPass\providers\SmsProvider
     * @deprecated
     */
    public $smsProvider;

    /**
     * 通行证租户服务提供者实例
     *
     * @var \iPass\providers\TenantProvider
     * @deprecated
     */
    public $tenantProvider;

    /**
     * 通行证用户数据管理者服务实例
     *
     * @var \iPass\providers\ManageProvider
     * @deprecated
     */
    public $manageProvider;

    /**
     * 通行证单点登录服务提供者实例
     *
     * @var \iPass\providers\SsoProvider
     * @deprecated
     */
    public $ssoProvider;

    /**
     * 通行证客户端接口服务提供者实例
     *
     * @var ClientProvider
     */
    public $client;

    /**
     * 通行证服务端接口服务提供者实例
     *
     * @var ServerProvider
     */
    public $server;

    /**
     * SDK 配置实例
     *
     * @var \iPass\Config
     */
    public $config;

    public function __construct($args = [])
    {
        // 初始化通行证配置类
        $config = isset($args['config']) ? $args['config'] : $args;
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config = $config;

        $this->userProvider   = UserProvider::instance($config);
        $this->tokenProvider  = TokenProvider::instance($config);
        $this->smsProvider    = SmsProvider::instance($config);
        $this->tenantProvider = TenantProvider::instance($config);

        // 实例化一个客户端服务接口调用类
        $this->client = new ClientProvider($config);
        // 实例化一个服务端服务接口调用类
        $this->server = new ServerProvider($config);
    }

    /**
     * @return ManageProvider
     * @deprecated
     */
    public function getManageProvider()
    {
        if (!is_null($this->manageProvider)) {
            return $this->manageProvider;
        }
        $this->manageProvider = ManageProvider::instance($this->config);
        return $this->manageProvider;
    }

    /**
     * 通行证客户端接口服务提供者
     *
     * @return ClientProvider
     */
    public function client()
    {
        return $this->client;
    }

    /**
     * 通行证服务端接口服务提供者
     *
     * @return ServerProvider
     */
    public function server()
    {
        return $this->server;
    }

    /**
     * @return SsoProvider
     * @deprecated
     */
    public function getSsoProvider()
    {
        if (!is_null($this->ssoProvider)) {
            return $this->ssoProvider;
        }
        $this->ssoProvider = SsoProvider::instance($this->config);
        return $this->ssoProvider;
    }

    /**
     * 获取通行证接口调用失败时接口返回的异常说明内容，接口报错未能正常响应时无法获取报错内容。
     * 多次调用接口后，会按照后进先出的模式优先获取队尾的异常内容信息
     *
     * @param  string $defaultMsg 获取异常信息失败时的默认值
     * @return string
     * @deprecated
     */
    public function getApiResponseFaildMsg($defaultMsg = '')
    {
        $failMsg = HttpHelper::popHttpBizFaildMsg();
        return !is_null($failMsg) ? $failMsg : $defaultMsg;
    }

    /**
     * 手机号+短信验证码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     *
     * @param  string $mobile       手机号
     * @param  string $smsCode      短信验证码
     * @param  string $tenantCode   指定登录的租户号
     * @param  string $deviceId     登录所使用的的设备号
     * @return Token|array
     * @throws UserLoginException
     * @deprecated
     */
    public function loginBySmsCode($mobile, $smsCode, $countryCode = '86', $tenantCode = '', $deviceId = '')
    {
        return $this->userProvider->loginBySmsCode($mobile, $smsCode, $countryCode, $tenantCode, $deviceId);
    }

    /**
     * 账号/手机号+密码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     *
     * @param  string $accountOrMobile  账号或手机号
     * @param  string $password         密码
     * @param  string $tenantCode       指定登录的租户号
     * @param  string $deviceId         登录所使用的设备号
     * @return Token|array
     * @throws UserLoginException
     * @deprecated
     */
    public function loginByPwd($accountOrMobile, $password, $tenantCode = '', $deviceId = '')
    {
        return $this->userProvider->loginByPwd($accountOrMobile, $password, $tenantCode, $deviceId);
    }

    /**
     * 利用登录接口返回的 ticket 完成二段登录，换取最终有效的授权令牌数据
     *
     * @param  string $ticket       登录接口拿到的 ticket 凭证
     * @param  string $tenantCode   需要登录的租户号
     * @param  string $deviceId     当前登录设备的设备号
     * @return Token|null
     * @deprecated
     */
    public function loginByTicket($ticket, $tenantCode, $deviceId = '')
    {
        return $this->userProvider->loginByTicket($ticket, $tenantCode, $deviceId);
    }

    /**
     * 利用已登录的通行证租户授权 Token，换取另一个租户的登录 Token。Token 换
     * Token 的登录模式，不会将之前的 Token 失效，两个 Token 均可使用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $tenantCode   需要登录的租户
     * @return Token|null
     * @deprecated
     */
    public function loginByToken($accessToken, $tenantCode)
    {
        return $this->userProvider->loginByToken($accessToken, $tenantCode);
    }

    /**
     * 注册一个新的通行证用户账号，初始密码可以设置也可以不设置，没有设置初始密码的账号需要
     * 后续自行通过重置密码来设置一个登陆密码
     *
     * @param  string $countryCode  手机区号
     * @param  string $mobile       手机号
     * @param  string $smsCode      登陆类型的短信验证码
     * @param  string $password     登陆初始密码
     * @param  string $deviceId     设备号
     * @return bool
     * @deprecated
     */
    public function register($mobile, $smsCode, $countryCode = '86', $password = '', $deviceId = '')
    {
        return $this->userProvider->register($mobile, $smsCode, $countryCode, $password, $deviceId);
    }

    /**
     * 验证通行证中心颁发的访问令牌目前是否有效
     *
     * @param  string $accessToken 访问令牌
     * @return bool
     * @deprecated
     */
    public function validateAccessToken($accessToken)
    {
        return $this->tokenProvider->validate($accessToken);
    }

    /**
     * 利用通行证中心颁发的刷新令牌获取（新的）有效授权令牌
     *
     * @param  string $refreshToken 刷新令牌
     * @return Token|null
     * @deprecated
     */
    public function refreshToken($refreshToken)
    {
        return $this->tokenProvider->refresh($refreshToken);
    }

    /**
     * 根据通行证中心颁发的访问令牌获取对应的通行证用户身份
     *
     * @param  string $accessToken 访问令牌
     * @return User|null
     * @deprecated
     */
    public function getUnifiedPassUser($accessToken)
    {
        return $this->userProvider->load($accessToken);
    }

    /**
     * 更新通行证用户的基本信息。支持更新的内容有：账号、昵称、真实姓名、邮箱、性别
     *
     * @param  string $accessToken  访问令牌
     * @param  User   $user         通行证用户实例
     * @return bool
     * @deprecated
     */
    public function updateUser($accessToken, User $user)
    {
        return $this->userProvider->update($accessToken, $user);
    }

    /**
     * 修改通行证用户在租户下的授权认证状态
     *
     * @param  string $accessToken  审核人的访问令牌
     * @param  string $unionId      申请人的 unionId
     * @param  string $tenantCode   租户号
     * @param  int    $status       认证状态
     * @deprecated
     */
    public function changeApproval($accessToken, $unionId, $tenantCode, $status)
    {
        $this->userProvider->changeApproval($accessToken, $unionId, $tenantCode, $status);
    }

    /**
     * 获取通行证用户关联租户授权数据，访问令牌或临时登录凭证都能
     * 用于该接口数据的调用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $ticket       临时登录凭证
     * @return array
     * ```
     * [
     *   [
     *     'tenantName' => '租户名',
     *     'tenantCode' => '租户号',
     *     'approveStatus' => 2, // 认证状态
     *     'status' => 1, // 禁用|启用
     *   ],
     *   ...
     * ]
     * ```
     * @deprecated
     */
    public function getUserRelateTenantAuths($accessToken = '', $ticket = '')
    {
        return $this->userProvider->getRelateTenantAuths($accessToken, $ticket);
    }

    /**
     * 初始化设置通行证用户的登录密码，仅在用户未设置过登录密码，密码为空的时候可以调用
     *
     * @param  string $initPasswordTicket   登录接口获取到的初始化密码临时凭证
     * @param  string $password             登录密码
     * @return bool
     * @deprecated
     */
    public function initPassword($initPasswordTicket, $password)
    {
        return $this->userProvider->initPassword($initPasswordTicket, $password);
    }

    /**
     * 获取通行证用户重置密码所需 Ticket 凭证
     *
     * @param  string $mobile       手机号
     * @param  int    $smsCode      修改密码类型的短信验证码
     * @param  string $countryCode  手机区号
     * @return string
     * @deprecated
     */
    public function getResetPasswordTicket($mobile, $smsCode, $countryCode = '86')
    {
        return $this->userProvider->getResetPasswordTicket($mobile, $smsCode, $countryCode);
    }

    /**
     * 重置通行证用户的登录密码
     *
     * @param  string $ticket   修改密码凭证
     * @param  string $password 新密码
     * @return bool
     * @deprecated
     */
    public function resetPassword($ticket, $password)
    {
        return $this->userProvider->resetPassword($ticket, $password);
    }

    /**
     * 修改通行证用户的登录密码
     *
     * @param  string $accessToken      访问令牌
     * @param  string $oldPassword      旧密码
     * @param  string $newPassword      新密码
     * @param  string $reNewPassword    二次确认新密码
     * @return bool
     * @deprecated
     */
    public function changePassword($accessToken, $oldPassword, $newPassword, $reNewPassword)
    {
        return $this->userProvider->changePassowrd($accessToken, $oldPassword, $newPassword, $reNewPassword);
    }

    /**
     * 通行证用户绑定手机号。通过账号密码登录时，如果用户没有绑定手机号，需要用户
     * 绑定手机号后才能登录成功
     *
     * @param  string $ticket       登录临时凭证，
     * @param  string $mobile       需要绑定的手机号
     * @param  int    $smsCode      绑定手机号类型短信验证码
     * @param  string $countryCode  手机区号
     * @return bool
     * @deprecated
     */
    public function bindMobile($ticket, $mobile, $smsCode, $countryCode = '86')
    {
        return $this->userProvider->bindMobile($ticket, $mobile, $smsCode, $countryCode);
    }

    /**
     * 向统一通行证初始化用户数据，初始化成功后，将会带上用户对应的通行证
     * UnionID 数据返回。用户数组 $unifiedPassUsers 中每个用户需要提
     * 供的数据必须要有 [mobile|account|email] 三者其一，除此之外
     * nickname、realname、gender、avatarUrl、password 等参数选填。
     * 1. 选填属性如果通行证中对应用户的属性内容不为空，则不会被更新，以
     * 通行证的数据为准。
     * 2. 需要注意的是，通过该方法同步过的通行证用户，无论是新增还是已存
     * 在的用户都会被修改为认证通过的状态
     *
     * @deprecated
     * @param  array  $initUsers    需要初始化到通行证的用户数据数组
     * @param  string $tenantCode   租户号
     * @param  array  &$faildUsers  如果同步失败的用户，数据将放在这个引用参数中
     * @return array
     * ```
     *  [
     *      ... $initUser 中提供的用户数据,
     *      'unifiedPassUser' => \common\utils\User
     *  ]
     * ```
     * @deprecated
     */
    public function initUnifiedPassUsers(array $unifiedPassUsers, $tenantCode, &$faildUsers = [])
    {
        return $this->userProvider->initUsers($unifiedPassUsers, $tenantCode, $faildUsers);
    }

    /**
     * 发送统一通行证短信验证码，通行证支持的验证码类型有：登录验证码、注册验证码、
     * 修改（重置）密码验证码、绑定手机号验证码。短信发送有频率限制，具体得限制规则
     * 可以 @see https://confluence.mysre.cn/pages/viewpage.action?pageId=10915382
     *
     * @param  string $to   接收验证码的手机号
     * @param  string $type 短信验证码类型，可以通过 \iPass\support\SmsCodeType 获得
     * @return bool
     * @throws \InvalidArgumentException
     * @deprecated
     */
    public function sendSmsCode($to, $type)
    {
        return $this->smsProvider->sendCode($to, $type);
    }

    /**
     * 通行证搜索租户，只有能通过通行证搜索到的租户，才能被用于简历用户授权关联
     *
     * @param  string $tenantCode   租户号
     * @param  string $platform     平台标识
     * @return array ['name' => '租户名', 'code' => '租户号']
     * @deprecated
     */
    public function searchTenant($tenantCode, $platform = 'yunfuwu')
    {
        return $this->tenantProvider->search($tenantCode, $platform);
    }
}
