<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年06月21日16:42:49
 */
namespace iPass;

use iPass\struct\Token;

class LoginResponse extends ResponsePlus
{
    /**
     * 是否登录成功。根据接口返回的 data 数据中是否存在授权令牌数据来
     * 判断是否成功
     *
     * @return bool
     */
    public function isLoginSuccess()
    {
        return $this->existKeyInData('accessToken');
    }

    /**
     * 是否预登陆成功。预登陆成功指的是没有提供租户号，无法直接登录拿到
     * 与租户登录态相关联的授权令牌数据，仅仅只拿到了一个 ticket 凭证
     *
     * @return bool
     */
    public function isPreLoginSuccess()
    {
        return $this->existKeyInData('ticket');
    }

    /**
     * 登录成功后，将接口返回数据转换成授权令牌实例
     *
     * @return Token
     */
    public function toToken()
    {
        return new Token($this->data);
    }

    /**
     * 判断当前登录失败是否因为用户未设置登录密码
     *
     * @return bool
     */
    public function isNotSetLoginPassword()
    {
        return $this->errcode === 10017;
    }

    /**
     * 判断当前登录失败是否因为用户手机号未验证真实性
     *
     * @return bool
     */
    public function isNotVerifyMobile()
    {
        return $this->errcode === 10036;
    }

    /**
     * 判断当前登录失败是否因为用户未绑定手机号
     *
     * @return bool
     */
    public function isNotBindMobile()
    {
        return $this->errcode === 10035;
    }
}
