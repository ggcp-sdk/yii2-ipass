<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\struct;

use iPass\support\traits\ArrayableTrait;
use iPass\support\traits\LoadPropsTrait;

class Token
{
    use LoadPropsTrait;
    use ArrayableTrait;
    
    /**
     * 访问令牌
     *
     * @var string
     */
    public $accessToken;

    /**
     * 刷新令牌
     *
     * @var string
     */
    public $refreshToken;

    /**
     * 访问令牌剩余有效时长，单位：s（秒）
     *
     * @var int
     */
    public $expireTime;

    /**
     * 刷新令牌剩余有效时长，单位：s（秒）
     *
     * @var int
     */
    public $refreshTime;

    /**
     * 获取到令牌时的时间戳
     * 
     * @var int
     */
    private $getAt;

    public function __construct($props = [])
    {
        $this->loadProps($props);
        $this->getAt = time();
    }

    /**
     * 访问令牌是否已过期
     * 
     * @return bool
     */
    public function isExpiredAccessToken()
    {
        return ($this->expireTime + $this->getAt) < time();
    }
}
