<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\struct;

use ArrayAccess;
use iPass\support\traits\ArrayableTrait;
use iPass\support\traits\LoadPropsTrait;

class User implements ArrayAccess
{
    use LoadPropsTrait;
    use ArrayableTrait;
    
    /**
     * 通行证用户与租户应用授权绑定的唯一 ID
     *
     * @var string
     */
    public $openId;

    /**
     * 通行证用户唯一 ID
     *
     * @var string
     */
    public $unionId;

    /**
     * 用户名、账号
     *
     * @var string
     */
    public $username = '';

    /**
     * 手机区号
     *
     * @var int
     */
    public $countryCode;

    /**
     * 手机号
     *
     * @var int
     */
    public $mobile;

    /**
     * 昵称
     *
     * @var string
     */
    public $nickname = '';

    /**
     * 真实姓名
     *
     * @var string
     */
    public $realname = '';

    /**
     * 邮箱地址
     *
     * @var string
     */
    public $email = '';

    /**
     * 头像图片地址
     *
     * @var string
     */
    public $avatarUrl = '';

    /**
     * 性别：0-未知、1-男、2-女
     *
     * @var int
     */
    public $gender = 0;

    /**
     * 手机号是否已验证：0-未验证、1-已验证
     * 
     * @var int
     */
    public $isVerified = 0;

    /**
     * 通行证用户登录的应用 AppID
     *
     * @var string
     */
    public $appid = '';

    /**
     * 通行证用户在登录应用上使用的租户号
     */
    public $tenantCode = '';

    /**
     * 通行证用户与租户的授权认证关系
     * 
     * @var int
     */
    public $approveStatus;

    public function __construct($props = [])
    {
        $this->loadProps(is_array($props) ? $props : []);
    }
}
