<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年06月21日16:01:42
 */
namespace iPass\client;

class ClientApis
{
    const API_USER_LOGIN            = '/v2/user/login';
    const API_USER_TICKET_LOGIN     = '/v2/user/login-by-ticket';
    const API_USER_TOKEN_LOGIN      = '/v2/user/login-by-token';
    const API_USER_REGISTER         = '/v2/user/register';
    const API_TOKEN_VALIDATE        = '/v2/token/validate';
    const API_TOKEN_REFRESH         = '/v2/token/refresh';
    const API_USER_INFO             = '/v2/user/info';
    const API_USER_UPDATE           = '/v2/user/update';
    const API_USER_APPROVE          = '/v2/user/approve';
    const API_USER_TENANT_AUTH      = '/v2/user/relate-tenant-list';
    const API_USER_CHANGE_PWD       = '/v2/user/change-password';
    const API_USER_RESET_PWD_TICKET = '/v2/user/pre-reset-password';
    const API_USER_RESET_PWD        = '/v2/user/reset-password';
    const API_INIT_USER_PWD         = '/v2/user/init-password';
    const API_USER_BIND_MOBILE      = '/v2/user/bind-mobile';
    const API_SEND_SMS_CODE         = '/v1/sms/send-code';
    const API_SEARCH_TENANT         = '/v2/tenant/search';
    const API_SSO_LOGIN             = '/v2/sso/login';
}
