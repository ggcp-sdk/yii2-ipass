<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年06月21日16:10:22
 */
namespace iPass\client;

use iPass\Config;
use iPass\ResponsePlus;
use iPass\struct\Token;
use iPass\struct\User;
use iPass\support\LoginType;
use iPass\support\SignatureHelper;
use iPass\support\SmsCodeType;

class ClientProvider
{
    /**
     * 通行证配置实例
     *
     * @var Config
     */
    public $config;

    /**
     * @var ClientSender
     */
    protected $sender;

    public function __construct($config = [])
    {
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config = $config;
        $this->sender = new ClientSender($config);
    }

    /**
     * 验证访问令牌是否有效
     *
     * @param  string $accessToken  访问令牌
     * @return ResponsePlus
     */
    public function validateAccessToken($accessToken)
    {
        $res = $this->sender->invokeValidateTokenApi($accessToken);
        return $res->isSuccess();
    }

    /**
     * 利用刷新令牌获取（新的）有效授权令牌
     *
     * @param  string $refreshToken 刷新令牌
     * @return ResponsePlus
     */
    public function refreshToken($refreshToken)
    {
        $data = [
            'refreshToken' => $refreshToken,
            'appid'        => $this->config->appid,
            'appKey'       => $this->config->appKey,
            'timestamp'    => time(),
            'nonce'        => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeRefreshTokenApi($data);
    }

    /**
     * 手机号+短信验证码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     *
     * @param  string $mobile       手机号
     * @param  int    $smsCode      短信验证码
     * @param  string $countryCode  手机区号
     * @param  string $tenantCode   指定登录的租户号
     * @param  string $deviceId     登录所使用的的设备号
     * @return ResponsePlus
     * @throws UserLoginException
     */
    public function loginBySmsCode($mobile, $smsCode, $countryCode = '86', $tenantCode = '', $deviceId = '')
    {
        $data = [
            'loginType'   => LoginType::TYPE_MOBILE_AND_SMS,
            'appid'       => $this->config->appid,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'tenantCode'  => $tenantCode,
        ];
        return $this->sender->invokeLoginApi($data, $deviceId);
    }

    /**
     * 发送统一通行证短信验证码，通行证支持的验证码类型有：登录验证码、注册验证码、
     * 修改（重置）密码验证码、绑定手机号验证码。短信发送有频率限制
     *
     * @param  string $to   接收验证码的手机号
     * @param  string $type 短信验证码类型，可以通过 \iPass\support\SmsCodeType 获得
     * @return ResponsePlus
     * @throws \InvalidArgumentException
     */
    public function sendCode($to, $type, $tenantCode = '')
    {
        if (!SmsCodeType::checkType($type)) {
            throw new \InvalidArgumentException("不支持的通行证验证码短信类型 {$type}");
        }

        $data = [
            'to'     => $to,
            'type'   => $type,
            'appid'  => $this->config->appid,
            'appKey' => $this->config->appKey,
        ];
        if (!empty($tenantCode)) {
            $data['tenantCode'] = $tenantCode;
        }

        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeSendSmsCodeApi($data);
    }

    /**
     * 账号/手机号+密码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     * ps: 通过密码登录时，如果登录的用户未绑定手机号，也会返回登录临时凭证
     *
     * @param  string $accountOrMobile  账号或手机号
     * @param  string $password         密码
     * @param  string $tenantCode       指定登录的租户号
     * @param  string $deviceId         登录所使用的设备号
     * @return ResponsePlus
     * @throws UserLoginException
     */
    public function loginByPassword($accountOrMobile, $password, $tenantCode = '', $deviceId = '')
    {
        $data = [
            'loginType'  => LoginType::TYPE_ACCOUNT_AND_PWD,
            'appid'      => $this->config->appid,
            'account'    => $accountOrMobile,
            'password'   => $password,
            'tenantCode' => $tenantCode,
        ];

        return $this->sender->invokeLoginApi($data, $deviceId);
    }

    /**
     * 利用登录接口返回的 ticket 完成二段登录，换取最终有效的授权令牌数据
     *
     * @param  string $ticket       登录接口拿到的 ticket 凭证
     * @param  string $tenantCode   需要登录的租户号
     * @param  string $deviceId     当前登录设备的设备号
     * @return ResponsePlus
     */
    public function loginByTicket($ticket, $tenantCode, $deviceId = '')
    {
        $data = [
            'ticket'     => $ticket,
            'tenantCode' => $tenantCode,
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeLoginByTicketApi($data, $deviceId);
    }

    /**
     * 利用已登录的通行证租户授权 Token，换取另一个租户的登录 Token。Token 换
     * Token 的登录模式，不会将之前的 Token 失效，两个 Token 均可使用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $tenantCode   需要登录的租户
     * @return ResponsePlus
     */
    public function loginByToken($accessToken, $tenantCode)
    {
        $data = [
            'accessToken' => $accessToken,
            'tenantCode'  => $tenantCode,
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeLoginByTokenApi($data);
    }

    /**
     * 注册一个新的通行证用户账号，初始密码可以设置也可以不设置，没有设置初始密码的账号需要
     * 后续自行通过重置密码来设置一个登陆密码
     *
     * @param  string $countryCode  手机区号
     * @param  string $mobile       手机号
     * @param  string $smsCode      登陆类型的短信验证码
     * @param  string $password     登陆初始密码
     * @param  string $deviceId     设备号
     * @param  string $tenantCode   租户号
     * @return ResponsePlus
     */
    public function register($mobile, $smsCode, $countryCode = '86', $password = '', $deviceId = '', $tenantCode = '')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'mobile'      => $mobile,
            'countryCode' => $countryCode,
            'smsCode'     => $smsCode,
            'password'    => $password,
            'tenantCode'  => $tenantCode,
        ];

        unset($data['appKey']);

        return $this->sender->invokeRegisterApi($data);
    }

    /**
     * 根据访问令牌获取对应的通行证用户身份
     *
     * @param  string $accessToken  访问令牌
     * @return ResponsePlus
     */
    public function getUser($accessToken)
    {
        return $this->sender->invokeUserInfoApi($accessToken);
    }

    /**
     * 更新通行证用户的基本信息。支持更新的内容有：账号、昵称、真实姓名、邮箱、性别
     *
     * @param  string $accessToken  访问令牌
     * @param  User   $user         通行证用户实例
     * @return ResponsePlus
     */
    public function updateUser($accessToken, User $user)
    {
        $data = [
            'username' => $user->username,
            'nickname' => $user->nickname,
            'realname' => $user->realname,
            'email'    => $user->email,
            'gender'   => $user->gender,
        ];

        return $this->sender->invokeUpdateUserApi($accessToken, $data);
    }

    /**
     * 获取通行证用户关联租户授权数据，访问令牌或临时登录凭证都能
     * 用于该接口数据的调用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $ticket       临时登录凭证
     * @return ResponsePlus
     */
    public function getUserTenantRelations($accessToken = '', $ticket = '')
    {
        $params = ['accessToken' => $accessToken, 'ticket' => $ticket];
        return $this->sender->invokeUserAuthRelationApi($params);
    }

    /**
     * 变更用户与租户的授权认证状态
     *
     * @param  string $accessToken  审核人的访问令牌
     * @param  string $openId       申请人的 UnionId
     * @param  string $tenantCode   租户号
     * @param  int    $status       需要变更的状态
     * @return ResponsePlus
     */
    public function changeApproval($accessToken, $unionId, $tenantCode, $status)
    {
        $data = ['status' => $status, 'unionId' => $unionId, 'tenantCode' => $tenantCode];
        return $this->sender->invokeChangeApprovalApi($accessToken, $data);
    }

    /**
     * 初始化设置通行证用户的登录密码
     *
     * @param  string $initPasswordTicket   修改密码凭证
     * @param  string $password 登录密码
     * @param  string $tenantCode 租户号
     * @return ResponsePlus
     */
    public function initPassword($initPasswordTicket, $password, $tenantCode = '')
    {
        $data = [
            'initPasswordTicket' => $initPasswordTicket,
            'password' => $password,
            'tenantCode' => $tenantCode,
        ];
        return $this->sender->invokeInitPasswordApi($data);
    }

    /**
     * 获取通行证用户重置密码所需 Ticket 凭证
     *
     * @param  string $mobile       手机号
     * @param  int    $smsCode      修改密码类型的短信验证码
     * @param  string $countryCode  手机区号
     * @return ResponsePlus
     */
    public function getResetPasswordTicket($mobile, $smsCode, $countryCode = '86')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeGetResetPasswordTicketApi($data);
    }

    /**
     * 重置通行证用户的登录密码
     *
     * @param  string $ticket   修改密码凭证
     * @param  string $password 新密码
     * @param  string $tenantCode 租户号
     * @return ResponsePlus
     */
    public function resetPassword($ticket, $password, $tenantCode = '')
    {
        $data = [
            'ticket' => $ticket,
            'password' => $password,
            'tenantCode' => $tenantCode,
        ];
        return $this->sender->invokeResetPasswordApi($data);
    }

    /**
     * 修改通行证用户的登录密码
     *
     * @param  string $accessToken      访问令牌
     * @param  string $oldPassword      旧密码
     * @param  string $newPassword      新密码
     * @param  string $reNewPassword    二次确认新密码
     * @return ResponsePlus
     */
    public function changePassowrd($accessToken, $oldPassword, $newPassword, $reNewPassword)
    {
        $data = [
            'type'     => 'CHECK_PASSWORD',
            'oldPwd'   => $oldPassword,
            'newPwd'   => $newPassword,
            'reNewPwd' => $reNewPassword,
        ];

        return $this->sender->invokeChangePasswordApi($accessToken, $data);
    }

    /**
     * 通行证用户绑定手机号。通过账号密码登录时，如果用户没有绑定手机号，需要用户
     * 绑定手机号后才能登录成功
     *
     * @param  string $ticket       登录临时凭证，
     * @param  string $mobile       需要绑定的手机号
     * @param  int    $smsCode      绑定手机号类型短信验证码
     * @param  string $countryCode  手机区号
     * @return ResponsePlus
     */
    public function bindMobile($ticket, $mobile, $smsCode, $countryCode = '86')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'ticket'      => $ticket,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        return $this->sender->invokeBindMobileApi($data);
    }

    /**
     * 通行证搜索租户，只有能通过通行证搜索到的租户，才能被用于简历用户授权关联
     *
     * @param  string $tenantCode   租户号
     * @param  string $platform     平台标识
     * @return ResponsePlus
     */
    public function searchTenant($tenantCode, $platform = 'yunfuwu')
    {
        $params = ['tenantCode' => $tenantCode, 'platform' => $platform];
        return $this->sender->invokeSearchTenantApi($params);
    }
}
