<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 * @since 2021年08月31日15:15:05
 */
namespace iPass\client;

use GgcpHttp\Client;
use iPass\Config;
use iPass\ResponsePlus;
use iPass\struct\Token;
use iPass\support\facades\Log;
use iPass\support\HttpHelper;
use iPass\support\traits\SingletonTrait;

class ClientSender
{
    use SingletonTrait;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \GgcpHttp\Sender
     */
    private $senderEngine;

    /**
     * @var Token|null
     */
    private $token;

    public function __construct($config = [])
    {
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config       = $config;
        $this->senderEngine = Client::prepare(['baseUri' => $this->config->apiHost])->getSender();
    }

    /**
     * 调用登录服务接口
     *
     * @param  array  $data
     * @param  string $deviceId
     * @return ResponsePlush
     */
    public function invokeLoginApi(array $data, $deviceId = '')
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_LOGIN, ['deviceId' => $deviceId]);
        $response = $this->senderEngine->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array  $data
     * @param  string $deviceId
     * @return ResponsePlush
     */
    public function invokeLoginByTicketApi(array $data, $deviceId = '')
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_TICKET_LOGIN, ['deviceId' => $deviceId]);
        $response = $this->senderEngine->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeLoginByTokenApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_USER_TOKEN_LOGIN, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeRegisterApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_USER_REGISTER, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @return ResponsePlush
     */
    public function invokeUserInfoApi($accessToken)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_INFO, ['accessToken' => $accessToken]);
        $response = $this->senderEngine->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return ResponsePlush
     */
    public function invokeUpdateUserApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_UPDATE, ['accessToken' => $accessToken]);
        $response = $this->senderEngine->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $queryParams
     * @return ResponsePlush
     */
    public function invokeUserAuthRelationApi(array $queryParams = [])
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_TENANT_AUTH, $queryParams);
        $response = $this->senderEngine->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return ResponsePlush
     */
    public function invokeChangeApprovalApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_APPROVE, ['accessToken' => $accessToken]);
        $response = $this->senderEngine->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeInitPasswordApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_INIT_USER_PWD, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeGetResetPasswordTicketApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_USER_RESET_PWD_TICKET, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeResetPasswordApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_USER_RESET_PWD, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return ResponsePlush
     */
    public function invokeChangePasswordApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_USER_CHANGE_PWD, ['accessToken' => $accessToken]);
        $response = $this->senderEngine->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeBindMobileApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_USER_BIND_MOBILE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @return ResponsePlush
     */
    public function invokeValidateTokenApi($accessToken)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_TOKEN_VALIDATE, ['accessToken' => $accessToken]);
        $response = $this->senderEngine->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeRefreshTokenApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_TOKEN_REFRESH, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeSsoLoginApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_SSO_LOGIN, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeSendSmsCodeApi(array $data)
    {
        $response = $this->senderEngine->postWithJson(ClientApis::API_SEND_SMS_CODE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return ResponsePlush
     */
    public function invokeSearchTenantApi(array $queryParams)
    {
        $uri      = HttpHelper::buildQueryParams(ClientApis::API_SEARCH_TENANT, $queryParams);
        $response = $this->senderEngine->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @return ResponsePlush
     */
    protected function handleResponse(\GgcpHttp\Response $response)
    {
        $res = new ResponsePlus($response);
        if (!$res->isSuccess()) {
            Log::info('通行证接口调用失败: ' . $res->getMessage());
        }
        return $res;
    }
}
