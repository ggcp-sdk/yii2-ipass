<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass;

use iPass\exceptions\ConfigException;
use iPass\support\traits\LoadPropsTrait;

class Config
{
    use LoadPropsTrait;

    /******************* 客户端服务接口 ********************/
    
    /** 通行证用户登陆接口 */
    const API_USER_LOGIN = '/v2/user/login';
    /** 通行证用户 Ticket 登陆接口 */
    const API_USER_TICKET_LOGIN = '/v2/user/login-by-ticket';
    /** 通行证用户 Token 登陆接口（切换登录租户） */
    const API_USER_TOKEN_LOGIN = '/v2/user/login-by-token';
    /** 通行证用户注册接口 */
    const API_USER_REGISTER = '/v2/user/register';
    /** 验证通行证令牌是否有效接口 */
    const API_TOKEN_VALIDATE = '/v2/token/validate';
    /** 刷新通行证令牌接口 */
    const API_TOKEN_REFRESH = '/v2/token/refresh';
    /** 获取通行证用户身份信息接口 */
    const API_USER_INFO = '/v2/user/info';
    /** 通行证用户更新个人信息 */
    const API_USER_UPDATE = '/v2/user/update';
    /** 通行证用户租户授权认证接口 */
    const API_USER_APPROVE = '/v2/user/approve';
    /** 获取用户关联的租户授权数据 */
    const API_USER_TENANT_AUTH = '/v2/user/relate-tenant-list';
    /** 通行证用户修改密码接口 */
    const API_USER_CHANGE_PWD = '/v2/user/change-password';
    /** 重置密码前置身份校验接口 */
    const API_USER_RESET_PWD_TICKET = '/v2/user/pre-reset-password';
    /** 通行证用户重置密码接口 */
    const API_USER_RESET_PWD = '/v2/user/reset-password';
    /** 初始化设置用户登录密码 */
    const API_INIT_USER_PWD = '/v2/user/init-password';
    /** 通行证用户首次绑定手机号接口 */
    const API_USER_BIND_MOBILE = '/v2/user/bind-mobile';
    /** 发送短信验证码接口 */
    const API_SEND_SMS_CODE = '/v1/sms/send-code';
    /** 租户搜索接口 */
    const API_SEARCH_TENANT = '/v2/tenant/search';
    /** 单点登录接口 */
    const API_SSO_LOGIN = '/v2/sso/login';

    /** 服务端，系统调用数据管理接口（即将废弃） */

    /** 启用用户与租户的授权认证关系 */
    const API_USER_AUTH_ENABLE = '/v2/sys/enable-user-auth';
    /** 禁用用户与租户的授权认证关系  */
    const API_USER_AUTH_DISABLE = '/v2/sys/disable-user-auth';
    /** 删除用户与租户的授权认证关系 */
    const API_USER_AUTH_DELETE = '/v2/sys/delete-user-auth';
    /** 系统调用向通行证初始化用户信息 */
    const API_INIT_USERSE = '/v2/sys/init-users';
    /** 分页获取通行证用户基本信息 */
    const API_USER_LIST = '/v2/sys/user-list';
    /** 批量获取通行证用户的基本信息 */
    const API_BATCH_USER_INFO = '/v2/sys/batch-user-info';
    /** 通过 UnionID 批量获取某个租户下的通行证用户基本信息 */
    const API_BATCH_USER_INFO_BY_UNION_ID = '/v2/sys/batch-user-info-by-union-ids';
    /** 管理员修改用户基础信息 */
    const API_MODIFY_USER = '/v2/sys/modify-user';

    /** 接口返回状态码：令牌有效 */
    const API_SUCCESS = 0;
    /** 接口返回错误码：令牌已失效，但是有新的有效令牌返回 */
    const CODE_TOKEN_REFRESH_FAILD = 10022;

    /**
     * 统一通行证接口域名地址
     *
     * @var string
     */
    public $apiHost;

    /**
     * 应用 Code
     * 
     * @var string
     */
    public $appCode;

    /**
     * 应用 AppID
     *
     * @var string
     */
    public $appid;

    /**
     * 应用 AppKey
     *
     * @var string
     */
    public $appKey;

    /**
     * 配置数据
     *
     * @var array
     */
    public $config = [];

    public function __construct($config = [])
    {
        if (empty($config['apiHost'])) {
            throw new ConfigException('Config argument [apiHost] has not been set.');
        }
        if (empty($config['appid'])) {
            throw new ConfigException('Config argument [appid] has not been set.');
        }
        if (empty($config['appKey'])) {
            throw new ConfigException('Config argument [appKey] has not been set.');
        }

        $this->loadProps($config);

        $this->config = $config;
    }

    /**
     * 根据配置 Key 获取对应的数据值
     *
     * @param  string $key  配置 key
     * @return mixed
     */
    public function get($key, $defaultValue = null)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        return $defaultValue;
    }

    /**
     * 判断某个配置参数是否有设置
     *
     * @param  string $key 参数名
     * @return bool
     */
    public function hasSet($key)
    {
        if (!empty($this->{$key})) {
            return true;
        }

        return !empty($this->config[$key]);
    }
}
