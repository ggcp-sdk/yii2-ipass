<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass;

use yii\helpers\ArrayHelper;

class Response
{
    /**
     * @var int
     */
    protected $errcode;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var mixed
     */
    protected $data;

    public function __construct(array $apiRes)
    {
        $this->errcode = isset($apiRes['errcode']) ? (int) $apiRes['errcode'] : 10000;
        $this->message = $apiRes['message'] ?? 'Unknown';
        $this->data    = $apiRes['data'] ?? [];
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->errcode === Config::API_SUCCESS;
    }

    /**
     * @return int
     */
    public function getErrCode()
    {
        return $this->errcode;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param  string $key
     * @return bool
     */
    public function existKeyInData($key)
    {
        return ArrayHelper::keyExists($key, $this->data);
    }

    /**
     * 从接口返回的 data 字段数据中获取指定 key 的数据内容。支持 key1.key2 的深度获取
     *
     * @param  string $key
     * @param  mixed  $defaultValue
     * @return mixed
     */
    public function getValueInData($key, $defaultValue = null)
    {
        return ArrayHelper::getValue($this->data, $key, $defaultValue);
    }
}
