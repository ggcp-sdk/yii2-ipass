<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年06月21日17:03:37
 */
namespace iPass;

use iPass\struct\User;

class UserResponse extends ResponsePlus
{
    /**
     * 将接口返回的用户数据转换成对应的通行证用户实例
     *
     * @return User
     */
    public function toUser()
    {
        return new User($this->data);
    }
}
