<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年06月21日16:33:03
 */
namespace iPass;

class ResponsePlus extends Response
{
    /**
     * 接口响应数据实例
     *
     * @var \GgcpHttp\Response
     */
    private $ggcpHttpResponse;

    public function __construct($ggcpHttpResponse)
    {
        if ($ggcpHttpResponse instanceof \GgcpHttp\Response) {
            $this->ggcpHttpResponse = $ggcpHttpResponse;
            parent::__construct($this->ggcpHttpResponse->toArray());
        }
    }

    /**
     * @return LoginResponse
     */
    public function asLoginResponse()
    {
        return new LoginResponse($this->ggcpHttpResponse);
    }

    /**
     * @return UserResponse
     */
    public function asUserResponse()
    {
        return new UserResponse($this->ggcpHttpResponse);
    }
}
