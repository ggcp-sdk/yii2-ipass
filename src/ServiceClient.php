<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass;

use GgcpHttp\Client;
use iPass\exceptions\NotBindUserMobileException;
use iPass\exceptions\NotSetUserPwdException;
use iPass\exceptions\NotVerifyUserMobileException;
use iPass\Response;
use iPass\support\facades\Log;
use iPass\support\HttpHelper;
use iPass\support\traits\SingletonTrait;

/**
 * @method static $this instance(string $thos)
 */
class ServiceClient
{
    use SingletonTrait;

    /**
     * @var \GgcpHttp\Sender
     */
    private $sender;

    public function __construct($host)
    {
        $this->sender = Client::prepare(['baseUri' => $host])->getSender();
    }

    /**
     * 调用登录服务接口
     *
     * @param  array  $data
     * @param  string $deviceId
     * @return Response
     */
    public function invokeLoginApi(array $data, $deviceId = '')
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_LOGIN, ['deviceId' => $deviceId]);
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array  $data
     * @param  string $deviceId
     * @return Response
     */
    public function invokeLoginByTicketApi(array $data, $deviceId = '')
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_TICKET_LOGIN, ['deviceId' => $deviceId]);
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeLoginByTokenApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_TOKEN_LOGIN, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeRegisterApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_REGISTER, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @return Response
     */
    public function invokeUserInfoApi($accessToken)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_INFO, ['accessToken' => $accessToken]);
        $response = $this->sender->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return Response
     */
    public function invokeUpdateUserApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_UPDATE, ['accessToken' => $accessToken]);
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $queryParams
     * @return Response
     */
    public function invokeUserAuthRelationApi(array $queryParams = [])
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_TENANT_AUTH, $queryParams);
        $response = $this->sender->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return Response
     */
    public function invokeChangeApprovalApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_APPROVE, ['accessToken' => $accessToken]);
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeInitUsersApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_INIT_USERSE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeInitPasswordApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_INIT_USER_PWD, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeGetResetPasswordTicketApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_RESET_PWD_TICKET, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeResetPasswordApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_RESET_PWD, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @param  array  $data
     * @return Response
     */
    public function invokeChangePasswordApi($accessToken, array $data)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_USER_CHANGE_PWD, ['accessToken' => $accessToken]);
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeBindMobileApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_BIND_MOBILE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  string $accessToken
     * @return Response
     */
    public function invokeValidateTokenApi($accessToken)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_TOKEN_VALIDATE, ['accessToken' => $accessToken]);
        $response = $this->sender->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeRefreshTokenApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_TOKEN_REFRESH, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeSsoLoginApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_SSO_LOGIN, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeSendSmsCodeApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_SEND_SMS_CODE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeSearchTenantApi(array $queryParams)
    {
        $uri      = HttpHelper::buildQueryParams(Config::API_SEARCH_TENANT, $queryParams);
        $response = $this->sender->get($uri);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeEnableUserAuthApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_AUTH_ENABLE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeDisableUserAuthApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_AUTH_DISABLE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeDeleteUserAuthApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_USER_AUTH_DELETE, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeGetUsersByOpenIdsApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_BATCH_USER_INFO, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeGetUsersByUnionIdsApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_BATCH_USER_INFO_BY_UNION_ID, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  array $data
     * @return Response
     */
    public function invokeModifyUserApi(array $data)
    {
        $response = $this->sender->postWithJson(Config::API_MODIFY_USER, $data);
        return $this->handleResponse($response);
    }

    /**
     * @return Response
     * @throws NotSetUserPwdException
     * @throws NotBindUserMobileException
     * @throws NotVerifyUserMobileException
     */
    protected function handleResponse(\GgcpHttp\Response $response)
    {
        $res = $response->toArray();

        if (empty($res) || $res['errcode'] != Config::API_SUCCESS) {
            Log::warning('通行证接口调用失败: ' . $res['message']);
        }
        if ($res['errcode'] == 10017) {
            // 用户未设置登录密码
            throw new NotSetUserPwdException($res['message'], $res['errcode'], $res['data']);
        }
        if ($res['errcode'] == 10036) {
            // 用户手机号未验证
            throw new NotVerifyUserMobileException($res['message'], $res['errcode'], $res['data']);
        }
        if ($res['errcode'] == 10035) {
            // 用户未绑定手机号
            throw new NotBindUserMobileException($res['message'], $res['errcode'], $res['data']);
        }

        return new Response($res);
    }
}
