<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\struct\Token;
use iPass\support\SignatureHelper;

class SsoProvider extends BaseProvider
{
    /**
     * 通过通行证用户 UnionID 单点登录获取授权令牌数据
     *
     * @param  string $unionId      通行证用户 UnionID
     * @param  string $tenantCode   租户号
     * @return Token|null
     */
    public function loginByUnionId($unionId, $tenantCode)
    {
        $data = [
            'appid'      => $this->config->appid,
            'unionId'    => $unionId,
            'tenantCode' => $tenantCode,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];

        $data['signature']      = $this->generateSignature($data, $this->config->appKey);
        $data['clientIdentity'] = $this->generateClientIdentity();
        $data['ssoType']        = 'SIGNATURE';

        $res = $this->serviceClient->invokeSsoLoginApi($data);
        return $res->isSuccess() ? new Token($res->getData()) : null;
    }

    /**
     * 生成签名
     *
     * @param  array  $params   参与签名的参数
     * @param  string $secret   密钥
     * @return string
     */
    protected function generateSignature($params, $secret)
    {
        ksort($params);

        $str = '';
        foreach ($params as $k => $v) {
            $str .= "{$k}={$v}&";
        }

        return sha1(trim($str, '&') . $secret);
    }

    /**
     * 生成客户端身份凭证
     *
     * @return array
     */
    protected function generateClientIdentity()
    {
        $identity = [
            'appid'     => $this->config->appid,
            'timestamp' => time(),
            'nonce'     => SignatureHelper::generateNonce(),
        ];

        $identity['signature'] = $this->generateSignature($identity, $this->config->appKey);
        return $identity;
    }
}
