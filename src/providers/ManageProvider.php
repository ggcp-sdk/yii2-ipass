<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\Config;
use iPass\struct\User;
use iPass\support\HttpHelper;
use iPass\support\SignatureHelper;
use Yii;

/**
 * 通行证用户数据管理服务提供者
 */
class ManageProvider extends BaseProvider
{
    /**
     * 向通行证初始化用户数据，初始化成功后，将会带上用户对应的通行证
     * UnionID 数据返回。用户数组 $unifiedPassUsers 中每个用户需要提
     * 供的数据必须要有 [mobile|account|email] 三者其一，除此之外
     * nickname、realname、gender、avatarUrl、password 等参数选填。
     * 1. 选填属性如果通行证中对应用户的属性内容不为空，则不会被更新，以
     * 通行证的数据为准。
     * 2. 需要注意的是，通过该方法同步过的通行证用户，无论是新增还是已存
     * 在的用户都会被修改为认证通过的状态
     *
     * @param  array  $initUsers    需要初始化到通行证的用户数据数组
     * @param  string $tenantCode   租户号
     * @param  array  &$faildUsers  如果同步失败的用户，数据将放在这个引用参数中
     * @return array
     * ```
     *  [
     *      ... $initUser 中提供的用户数据,
     *      'unifiedPassUser' => \common\utils\User
     *  ]
     * ```
     */
    public function syncInitUsers(array $initUsers, $tenantCode, &$faildUsers = [])
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'tenantCode' => $tenantCode,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(16),
        ];
        $data['signature'] = SignatureHelper::signature($data);
        $data['initUsers'] = $initUsers;
        unset($data['appKey']);

        $res = $this->serviceClient->invokeInitUsersApi($data);
        if (!$res->isSuccess()) {
            return [];
        }

        $successUsers = $res->getValueInData('successUsers', []);
        $faildUsers   = $res->getValueInData('faildUsers', []);
        if (!empty($faildUsers)) {
            \Yii::error('部分通行证用户初始化失败：' . json_encode($faildUsers, JSON_UNESCAPED_UNICODE));
        }

        foreach ($successUsers as &$user) {
            if (!isset($user['unifiedPassUser']) || empty($user['unifiedPassUser'])) {
                \Yii::warning('初始化成功的通行证用户数据异常: ' . json_encode($user, JSON_UNESCAPED_UNICODE));
                continue;
            }
            $user['unifiedPassUser'] = new User($user['unifiedPassUser']);
        }
        return $successUsers;
    }

    public function asyncInitUsers()
    {
        # todo
    }

    public function getAsyncInitUsersProgress($batchId)
    {
        # todo
    }

    /**
     * 启用通行证用户
     *
     * @param  string $unionId      被启用用户的 UnionID
     * @param  string $tenantCode   租户号
     * @param  string $remark       操作说明备注
     * @return bool
     */
    public function enableUserAuth($unionId, $tenantCode, $remark = '')
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'unionId'    => $unionId,
            'tenantCode' => $tenantCode,
            'remark'     => $remark,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeEnableUserAuthApi($data);
        return $res->isSuccess();
    }

    /**
     * 禁用通行证用户
     *
     * @param  string $unionId      被禁用用户的 UnionID
     * @param  string $tenantCode   租户号
     * @param  string $remark       操作说明备注
     * @return bool
     */
    public function disableUserAuth($unionId, $tenantCode, $remark = '')
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'unionId'    => $unionId,
            'tenantCode' => $tenantCode,
            'remark'     => $remark,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeDisableUserAuthApi($data);
        return $res->isSuccess();
    }

    /**
     * 禁用通行证用户
     *
     * @param  string $unionId      被禁用用户的 UnionID
     * @param  string $tenantCode   租户号
     * @param  string $remark       操作说明备注
     * @return bool
     */
    public function deleteUserAuth($unionId, $tenantCode, $remark = '')
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'unionId'    => $unionId,
            'tenantCode' => $tenantCode,
            'remark'     => $remark,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeDeleteUserAuthApi($data);
        return $res->isSuccess();
    }

    /**
     * 根据提供的 OpenID 列表，批量获取对应的通行证用户数据，如果 OpenID 对应的授权
     * 记录不属于当前 APPID 所有，则不会返回相关数据
     *
     * @param  string[] $openIds    通行证用户 OpenID 数据数组
     * @return array
     */
    public function getUserListByOpenIds($openIds)
    {
        $data = [
            'appid'     => $this->config->appid,
            'appKey'    => $this->config->appKey,
            'timestamp' => time(),
            'nonce'     => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);
        $data['openIds']   = $openIds;

        unset($data['appKey']);

        $res = $this->serviceClient->invokeGetUsersByOpenIdsApi($data);
        return $res->isSuccess() ? $res->getValueInData('list') : [];
    }

    /**
     * 根据提供的 UnionID 列表，批量获取对应租户下的通行证用户数据，如果 UnionID
     * 在指定租户下没有当前应用的授权关系，则不会返回相关数据
     *
     * @param  string[] $unionIds   通行证用户 UnionID 数据数组
     * @param  string   $tenantCode 租户号
     * @return array
     */
    public function getUserListByUnionIds($unionIds, $tenantCode)
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'tenantCode' => $tenantCode,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);
        $data['unionIds']  = $unionIds;

        unset($data['appKey']);

        $res = $this->serviceClient->invokeGetUsersByUnionIdsApi($data);
        return $res->isSuccess() ? $res->getValueInData('list') : [];
    }

    /**
     * 管理员等角色编辑通行证用户基础信息，目前支持编辑的信息有：真实姓名(realname)
     * 、邮箱(email)
     *
     * @param  string $unionId      通行证用户 UnionID
     * @param  array  $modifyAttr   需要编辑的数据数组
     * @return bool
     */
    public function modifyUser($unionId, array $modifyAttr)
    {
        $data = [
            'appid'     => $this->config->appid,
            'appKey'    => $this->config->appKey,
            'unionId'   => $unionId,
            'realname'  => !empty($modifyAttr['realname']) ? $modifyAttr['realname'] : '',
            'email'     => !empty($modifyAttr['email']) ? $modifyAttr['email'] : '',
            'timestamp' => time(),
            'nonce'     => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeModifyUserApi($data);
        return $res->isSuccess();
    }
}
