<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\Config;
use iPass\exceptions\ConfigException;
use iPass\struct\Token;
use iPass\support\HttpHelper;
use iPass\support\SignatureHelper;
use Yii;

class TokenProvider extends BaseProvider
{
    /**
     * 验证访问令牌是否有效
     *
     * @param  string $accessToken  访问令牌
     * @return bool
     */
    public function validate($accessToken)
    {
        $res = $this->serviceClient->invokeValidateTokenApi($accessToken);
        return $res->isSuccess();
    }

    /**
     * 利用刷新令牌获取（新的）有效授权令牌
     *
     * @param  string $refreshToken 刷新令牌
     * @return Token|null
     */
    public function refresh($refreshToken)
    {
        $data = [
            'refreshToken' => $refreshToken,
            'appid'        => $this->config->appid,
            'appKey'       => $this->config->appKey,
            'timestamp'    => time(),
            'nonce'        => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeRefreshTokenApi($data);
        return $res->isSuccess() ? new Token($res->getData()) : null;
    }
}
