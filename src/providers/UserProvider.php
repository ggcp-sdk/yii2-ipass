<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\exceptions\UserLoginException;
use iPass\struct\Token;
use iPass\struct\User;
use iPass\support\LoginType;
use iPass\support\SignatureHelper;

class UserProvider extends BaseProvider
{
    /**
     * 手机号+短信验证码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     *
     * @param  string $mobile       手机号
     * @param  int    $smsCode      短信验证码
     * @param  string $countryCode  手机区号
     * @param  string $tenantCode   指定登录的租户号
     * @param  string $deviceId     登录所使用的的设备号
     * @return Token|array
     * @throws UserLoginException
     */
    public function loginBySmsCode($mobile, $smsCode, $countryCode = '86', $tenantCode = '', $deviceId = '')
    {
        $data = [
            'loginType'   => LoginType::TYPE_MOBILE_AND_SMS,
            'appid'       => $this->config->appid,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'tenantCode'  => $tenantCode,
        ];

        $res = $this->serviceClient->invokeLoginApi($data, $deviceId);
        if (!$res->isSuccess()) {
            throw new UserLoginException('通行证用户登陆失败: ' . $res->getMessage());
        }
        // 登录未提供指定的租户号，只能拿到登录 ticket 凭证，需要利用凭证进行二段登录
        return $res->existKeyInData('accessToken') ? (new Token($res->getData())) : $res->getData();
    }

    /**
     * 账号/手机号+密码的方式登录通行证。如果提供了指定有效的 $tenantCode，将会直接登录成功
     * 得到通行证授权令牌数据；如果未提供 $tenantCode 则只能拿到登录临时凭证 Ticket，通过进
     * 一步调用 loginByTicket 方法，可以得到通行证授权令牌
     * ps: 通过密码登录时，如果登录的用户未绑定手机号，也会返回登录临时凭证
     *
     * @param  string $accountOrMobile  账号或手机号
     * @param  string $password         密码
     * @param  string $tenantCode       指定登录的租户号
     * @param  string $deviceId         登录所使用的设备号
     * @return Token|array
     * @throws UserLoginException
     */
    public function loginByPwd($accountOrMobile, $password, $tenantCode = '', $deviceId = '')
    {
        $data = [
            'loginType'  => LoginType::TYPE_ACCOUNT_AND_PWD,
            'appid'      => $this->config->appid,
            'account'    => $accountOrMobile,
            'password'   => $password,
            'tenantCode' => $tenantCode,
        ];

        $res = $this->serviceClient->invokeLoginApi($data, $deviceId);
        if (!$res->isSuccess()) {
            throw new UserLoginException('通行证用户登陆失败: ' . $res->getMessage());
        }
        // 登录未提供指定的租户号，只能拿到登录 ticket 凭证，需要利用凭证进行二段登录
        return $res->existKeyInData('accessToken') ? (new Token($res->getData())) : $res->getData();
    }

    /**
     * 利用登录接口返回的 ticket 完成二段登录，换取最终有效的授权令牌数据
     *
     * @param  string $ticket       登录接口拿到的 ticket 凭证
     * @param  string $tenantCode   需要登录的租户号
     * @param  string $deviceId     当前登录设备的设备号
     * @return Token|null
     */
    public function loginByTicket($ticket, $tenantCode, $deviceId = '')
    {
        $data = [
            'ticket'     => $ticket,
            'tenantCode' => $tenantCode,
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeLoginByTicketApi($data, $deviceId);
        return $res->isSuccess() ? (new Token($res->getData())) : null;
    }

    /**
     * 利用已登录的通行证租户授权 Token，换取另一个租户的登录 Token。Token 换
     * Token 的登录模式，不会将之前的 Token 失效，两个 Token 均可使用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $tenantCode   需要登录的租户
     * @return Token|null
     */
    public function loginByToken($accessToken, $tenantCode)
    {
        $data = [
            'accessToken' => $accessToken,
            'tenantCode'  => $tenantCode,
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeLoginByTokenApi($data);
        return $res->isSuccess() ? (new Token($res->getData())) : null;
    }

    /**
     * 注册一个新的通行证用户账号，初始密码可以设置也可以不设置，没有设置初始密码的账号需要
     * 后续自行通过重置密码来设置一个登陆密码
     *
     * @param  string $countryCode  手机区号
     * @param  string $mobile       手机号
     * @param  string $smsCode      登陆类型的短信验证码
     * @param  string $password     登陆初始密码
     * @param  string $deviceId     设备号
     * @return bool
     */
    public function register($mobile, $smsCode, $countryCode = '86', $password = '', $deviceId = '')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'mobile'      => $mobile,
            'countryCode' => $countryCode,
            'smsCode'     => $smsCode,
            'password'    => $password,
        ];

        unset($data['appKey']);

        $res = $this->serviceClient->invokeRegisterApi($data);
        return $res->isSuccess();
    }

    /**
     * 根据访问令牌获取对应的通行证用户身份
     *
     * @param  string $accessToken  访问令牌
     * @return User|null
     */
    public function load($accessToken)
    {
        $res = $this->serviceClient->invokeUserInfoApi($accessToken);
        return $res->isSuccess() ? new User($res->getData()) : null;
    }

    /**
     * 更新通行证用户的基本信息。支持更新的内容有：账号、昵称、真实姓名、邮箱、性别
     *
     * @param  string $accessToken  访问令牌
     * @param  User   $user         通行证用户实例
     * @return bool
     */
    public function update($accessToken, User $user)
    {
        $data = [
            'username' => $user->username,
            'nickname' => $user->nickname,
            'realname' => $user->realname,
            'email'    => $user->email,
            'gender'   => $user->gender,
        ];

        $res = $this->serviceClient->invokeUpdateUserApi($accessToken, $data);
        return $res->isSuccess();
    }

    /**
     * 获取通行证用户关联租户授权数据，访问令牌或临时登录凭证都能
     * 用于该接口数据的调用
     *
     * @param  string $accessToken  访问令牌
     * @param  string $ticket       临时登录凭证
     * @return array
     * ```
     * [
     *   [
     *     'tenantName' => '租户名',
     *     'tenantCode' => '租户号',
     *     'approveStatus' => 2, // 认证状态
     *     'status' => 1, // 禁用|启用
     *   ],
     *   ...
     * ]
     * ```
     */
    public function getRelateTenantAuths($accessToken = '', $ticket = '')
    {
        $params = ['accessToken' => $accessToken, 'ticket' => $ticket];
        $res    = $this->serviceClient->invokeUserAuthRelationApi($params);
        return $res->isSuccess() ? $res->getValueInData('tenantList') : [];
    }

    /**
     * 变更用户与租户的授权认证状态
     *
     * @param  string $accessToken  审核人的访问令牌
     * @param  string $openId       申请人的 UnionId
     * @param  string $tenantCode   租户号
     * @param  int    $status       需要变更的状态
     * @return void
     */
    public function changeApproval($accessToken, $unionId, $tenantCode, $status)
    {
        $data = ['status' => $status, 'unionId' => $unionId, 'tenantCode' => $tenantCode];
        $this->serviceClient->invokeChangeApprovalApi($accessToken, $data);
    }

    /**
     * 向通行证初始化用户数据，初始化成功后，将会带上用户对应的通行证
     * UnionID 数据返回。用户数组 $unifiedPassUsers 中每个用户需要提
     * 供的数据必须要有 [mobile|account|email] 三者其一，除此之外
     * nickname、realname、gender、avatarUrl、password 等参数选填。
     * 1. 选填属性如果通行证中对应用户的属性内容不为空，则不会被更新，以
     * 通行证的数据为准。
     * 2. 需要注意的是，通过该方法同步过的通行证用户，无论是新增还是已存
     * 在的用户都会被修改为认证通过的状态
     *
     * @param  array  $initUsers    需要初始化到通行证的用户数据数组
     * @param  string $tenantCode   租户号
     * @param  array  &$faildUsers  如果同步失败的用户，数据将放在这个引用参数中
     * @return array
     * ```
     *  [
     *      ... $initUser 中提供的用户数据,
     *      'unifiedPassUser' => \common\utils\User
     *  ]
     * ```
     */
    public function initUsers(array $initUsers, $tenantCode, &$faildUsers = [])
    {
        $data = [
            'appid'      => $this->config->appid,
            'appKey'     => $this->config->appKey,
            'tenantCode' => $tenantCode,
            'timestamp'  => time(),
            'nonce'      => SignatureHelper::generateNonce(16),
        ];
        $data['signature'] = SignatureHelper::signature($data);
        $data['initUsers'] = $initUsers;

        unset($data['appKey']);

        $res = $this->serviceClient->invokeInitUsersApi($data);
        if (!$res->isSuccess()) {
            return [];
        }

        $successUsers = $res->getValueInData('successUsers', []);
        $faildUsers   = $res->getValueInData('faildUsers', []);
        if (!empty($faildUsers)) {
            \Yii::error('部分通行证用户初始化失败：' . json_encode($faildUsers, JSON_UNESCAPED_UNICODE));
        }

        foreach ($successUsers as &$user) {
            if (!isset($user['unifiedPassUser']) || empty($user['unifiedPassUser'])) {
                \Yii::warning('初始化成功的通行证用户数据异常: ' . json_encode($user, JSON_UNESCAPED_UNICODE));
                continue;
            }
            $user['unifiedPassUser'] = new User($user['unifiedPassUser']);
        }
        return $successUsers;
    }

    /**
     * 初始化设置通行证用户的登录密码
     *
     * @param  string $ticket   修改密码凭证
     * @param  string $password 登录密码
     * @return bool
     */
    public function initPassword($initPasswordTicket, $password)
    {
        $data = ['initPasswordTicket' => $initPasswordTicket, 'password' => $password];
        $res  = $this->serviceClient->invokeInitPasswordApi($data);
        return $res->isSuccess();
    }

    /**
     * 获取通行证用户重置密码所需 Ticket 凭证
     *
     * @param  string $mobile       手机号
     * @param  int    $smsCode      修改密码类型的短信验证码
     * @param  string $countryCode  手机区号
     * @return string
     */
    public function getResetPasswordTicket($mobile, $smsCode, $countryCode = '86')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeGetResetPasswordTicketApi($data);
        return $res->isSuccess() ? $res->getValueInData('ticket', '') : '';
    }

    /**
     * 重置通行证用户的登录密码
     *
     * @param  string $ticket   修改密码凭证
     * @param  string $password 新密码
     * @return bool
     */
    public function resetPassword($ticket, $password)
    {
        $data = ['ticket' => $ticket, 'password' => $password];
        $res  = $this->serviceClient->invokeResetPasswordApi($data);
        return $res->isSuccess();
    }

    /**
     * 修改通行证用户的登录密码
     *
     * @param  string $accessToken      访问令牌
     * @param  string $oldPassword      旧密码
     * @param  string $newPassword      新密码
     * @param  string $reNewPassword    二次确认新密码
     * @return bool
     */
    public function changePassowrd($accessToken, $oldPassword, $newPassword, $reNewPassword)
    {
        $data = [
            'type'     => 'CHECK_PASSWORD',
            'oldPwd'   => $oldPassword,
            'newPwd'   => $newPassword,
            'reNewPwd' => $reNewPassword,
        ];

        $res = $this->serviceClient->invokeChangePasswordApi($accessToken, $data);
        return $res->isSuccess();
    }

    /**
     * 通行证用户绑定手机号。通过账号密码登录时，如果用户没有绑定手机号，需要用户
     * 绑定手机号后才能登录成功
     *
     * @param  string $ticket       登录临时凭证，
     * @param  string $mobile       需要绑定的手机号
     * @param  int    $smsCode      绑定手机号类型短信验证码
     * @param  string $countryCode  手机区号
     * @return bool
     */
    public function bindMobile($ticket, $mobile, $smsCode, $countryCode = '86')
    {
        $data = [
            'appid'       => $this->config->appid,
            'appKey'      => $this->config->appKey,
            'ticket'      => $ticket,
            'countryCode' => $countryCode,
            'mobile'      => $mobile,
            'smsCode'     => $smsCode,
            'timestamp'   => time(),
            'nonce'       => SignatureHelper::generateNonce(),
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeBindMobileApi($data);
        return $res->isSuccess();
    }
}
