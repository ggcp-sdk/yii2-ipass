<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

class TenantProvider extends BaseProvider
{
    /**
     * 通行证搜索租户，只有能通过通行证搜索到的租户，才能被用于简历用户授权关联
     *
     * @param  string $tenantCode   租户号
     * @param  string $platform     平台标识
     * @return array ['name' => '租户名', 'code' => '租户号']
     */
    public function search($tenantCode, $platform = 'yunfuwu')
    {
        $params = ['tenantCode' => $tenantCode, 'platform' => $platform];
        $res    = $this->serviceClient->invokeSearchTenantApi($params);
        return $res->isSuccess() ? $res->getData() : [];
    }
}
