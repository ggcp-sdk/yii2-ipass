<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\Config;
use iPass\support\SignatureHelper;
use iPass\support\SmsCodeType;

class SmsProvider extends BaseProvider
{
    /**
     * 发送统一通行证短信验证码，通行证支持的验证码类型有：登录验证码、注册验证码、
     * 修改（重置）密码验证码、绑定手机号验证码。短信发送有频率限制
     *
     * @param  string $to   接收验证码的手机号
     * @param  string $type 短信验证码类型，可以通过 \iPass\support\SmsCodeType 获得
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function sendCode($to, $type)
    {
        if (!SmsCodeType::checkType($type)) {
            throw new \InvalidArgumentException("不支持的通行证验证码短信类型 {$type}");
        }

        $data = [
            'to'     => $to,
            'type'   => $type,
            'appid'  => $this->config->appid,
            'appKey' => $this->config->appKey,
        ];
        $data['signature'] = SignatureHelper::signature($data);

        unset($data['appKey']);

        $res = $this->serviceClient->invokeSendSmsCodeApi($data);
        return $res->isSuccess();
    }
}
