<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\providers;

use iPass\Config;
use iPass\ServiceClient;
use iPass\support\traits\SingletonTrait;

class BaseProvider
{
    use SingletonTrait;

    /**
     * 通行证配置实例
     *
     * @var \iPass\Config
     */
    public $config;

    /**
     * @var ServiceClient
     */
    protected $serviceClient;

    public function __construct($config = [])
    {
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config        = $config;
        $this->serviceClient = new ServiceClient($this->config->apiHost);
    }
}
