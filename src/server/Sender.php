<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 * @since 2021年08月31日15:15:05
 */
namespace iPass\server;

use GgcpHttp\Client;
use GgcpHttp\Response;
use iPass\Config;
use iPass\exceptions\ClientCredentialsTokenException;
use iPass\struct\Token;
use iPass\support\HttpHelper;
use iPass\support\traits\SingletonTrait;

class Sender
{
    use SingletonTrait;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \GgcpHttp\Sender
     */
    private $senderEngine;

    /**
     * @var Token|null
     */
    private $token;

    public function __construct($config = [])
    {
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config       = $config;
        $this->senderEngine = Client::prepare(['baseUri' => $this->config->apiHost])->getSender();
    }

    /**
     * 请求通行证批量创建用户接口
     *
     * @param  string $tenantCode 用户创建成功后，默认需要建立关联的租户号
     * @param  array $users 需要批量创建的用户数据数组
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeBatchCreateUsersApi($tenantCode, array $users)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::BATCH_CREATE_USER, ['accessToken' => $accessToken]);

        $data = [
            'tenantCode' => $tenantCode,
            'newUsers'   => $users,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证根据 UnionId 批量获取用户数据接口
     *
     * @param  string[] $unionIds 通行证用户 UnionId 数据数组
     * @param  string $tenantCode 租户号
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeGetUsersByUnionIdsApi(array $unionIds, $tenantCode = '')
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::USER_LIST_BY_UNIONID, ['accessToken' => $accessToken]);

        $data = [
            'tenantCode' => $tenantCode,
            'unionIds'   => $unionIds,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证更新用户信息接口
     *
     * @param  string $unionId 通行证用户 UnionId
     * @param  array $values 需要修改的用户信息数组
     * @param  string|null $remark 操作备注信息
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeUpdateUserApi($unionId, array $values, $remark = null)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::UPDATE_USER, ['accessToken' => $accessToken]);

        $data = [
            'unionId'   => $unionId,
            'realname'  => $values['realname'],
            'nickname'  => $values['nickname'],
            'email'     => $values['email'],
            'avatarUrl' => $values['avatarUrl'],
            'gender'    => $values['gender'],
            'remark'    => $remark,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证修改用户登陆密码接口
     *
     * @param  string $unionId 通行证用户 UnionId
     * @param  string $password 新的登录密码，可以是加密前的明文数据，也可以是加密后的密文数据
     * @param  string $isEncrypted 提供的密码参数，是加密前还是加密后的数据
     * @param  string $tenantCode 租户号
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeChangeUserPasswordApi($unionId, $password, $isEncrypted = false, $tenantCode = '')
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::CHANGE_PASSWORD, ['accessToken' => $accessToken]);

        $data = ['unionId' => $unionId, 'tenantCode' => $tenantCode];
        if ($isEncrypted) {
            $data['encryptedPassword'] = $password;
        } else {
            $data['password'] = $password;
        }

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证修改用户手机号接口
     *
     * @param  string $unionId      通行证用户 UnionId
     * @param  string $mobile       新的手机号
     * @param  int    $countryCode  新手机号的区号
     * @param  string|null $remark  操作备注信息
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeChangeUserMobileApi($unionId, $mobile, $countryCode = 86, $remark = null)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::CHANGE_MOBILE, ['accessToken' => $accessToken]);

        $data = [
            'unionId'     => $unionId,
            'mobile'      => $mobile,
            'countryCode' => $countryCode,
            'remark'      => $remark,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证启用用户与租户关联关系接口
     *
     * @param  string $tenantCode   租户号
     * @param  string $unionId      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeEnableUserTenantRelationApi($tenantCode, $unionId, $remark = null)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::ENABLE_USER_TENANT_RELATION, ['accessToken' => $accessToken]);

        $data = [
            'tenantCode' => $tenantCode,
            'unionId'    => $unionId,
            'remark'     => $remark,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证禁用用户与租户关联关系接口
     *
     * @param  string $tenantCode   租户号
     * @param  string $unionId      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeDisableUserTenantRelationApi($tenantCode, $unionId, $remark = null)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::DISABLE_USER_TENANT_RELATION, ['accessToken' => $accessToken]);

        $data = [
            'tenantCode' => $tenantCode,
            'unionId'    => $unionId,
            'remark'     => $remark,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证删除用户与租户关联关系接口
     *
     * @param  string $tenantCode   租户号
     * @param  string $unionId      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeDeleteUserTenantRelationApi($tenantCode, $unionId, $remark = null)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $url = HttpHelper::buildQueryParams(ServerApis::DELETE_USER_TENANT_RELATION, ['accessToken' => $accessToken]);

        $data = [
            'tenantCode' => $tenantCode,
            'unionId'    => $unionId,
            'remark'     => $remark,
        ];

        $response = $this->senderEngine->postWithJson($url, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用通行证获取用户与租户所有关联关系接口
     *
     * @param  string $unionId 通行证用户 UnionId
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    public function invokeUserTenantRelationsApi($unionId)
    {
        $accessToken = $this->getAccessToken();
        if (empty($accessToken)) {
            throw new ClientCredentialsTokenException('获取客户端凭证授权令牌失败');
        }

        $queryParams = ['accessToken' => $accessToken, 'unionId' => $unionId];

        $response = $this->senderEngine->get(ServerApis::USER_TENANT_RELATIONS, $queryParams);
        return $this->handleResponse($response);
    }

    /**
     * 获取服务端接口调用所需的客户端凭证访问令牌
     *
     * @return string
     */
    protected function getAccessToken()
    {
        if ($this->token && !$this->token->isExpiredAccessToken()) {
            return $this->token->accessToken;
        }

        $queryParams = [
            'appid'     => $this->config->appid,
            'appKey'    => $this->config->appKey,
            'grantType' => 'client_credentials',
        ];

        $response = $this->senderEngine->get(ServerApis::GET_TOKEN, $queryParams);
        if (!$response->isSuccess(true)) {
            $this->token = null;
            return '';
        }

        $resData     = $response->toArray();
        $this->token = new Token($resData['data']);
        return $this->token->accessToken;
    }

    /**
     * @return Response
     * @throws ClientCredentialsTokenException
     */
    protected function handleResponse(Response $response)
    {
        return $response;
    }
}
