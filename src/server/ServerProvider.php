<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 * since 2021年08月31日11:07:23
 */
namespace iPass\server;

use iPass\Config;
use iPass\struct\User;
use iPass\support\facades\Log;

class ServerProvider
{
    /**
     * 通行证配置实例
     *
     * @var Config
     */
    public $config;

    /**
     * @var Sender
     */
    protected $sender;

    public function __construct($config = [])
    {
        if (!($config instanceof \iPass\Config)) {
            $config = new Config($config);
        }

        $this->config = $config;
        $this->sender = new Sender($config);
    }

    /**
     * 根据通行证用户的 UnionId 数组获取多个通行证用户数据信息。
     * 当一次提供的 UnionId 数据超过 500 个时，会触发分批获取数据的逻辑，每批次获取 500 条数据
     *
     * @param  string[] $unionIds 通行证用户 UnionId 数组
     * @param  string $tenantCode 租户号
     * @return User[]
     */
    public function getUsersByUnionIds(array $unionIds, $tenantCode)
    {
        if (empty($unionIds)) {
            return [];
        }

        if (count($unionIds) > 500) {
            $users       = [];
            $unionIdsArr = array_chunk($unionIds, 500);
            foreach ($unionIdsArr as $ids) {
                $tmpUsers = $this->internalGetUsersByUnionIds($ids, $tenantCode);
                if (!empty($tmpUsers)) {
                    $users = array_merge($users, $tmpUsers);
                }
            }
            return $users;
        }
        return $this->internalGetUsersByUnionIds($unionIds, $tenantCode);
    }

    protected function internalGetUsersByUnionIds(array $unionIds, $tenantCode = null)
    {
        $response = $this->sender->invokeGetUsersByUnionIdsApi($unionIds, $tenantCode);
        if (!$response->isSuccess(true)) {
            return [];
        }

        $resData = $response->toArray();
        $users   = $resData['data']['list'] ?? [];

        $result = [];
        foreach ($users as $u) {
            $result[] = new User($u);
        }
        return $result;
    }

    /**
     * 批量创建通行证用户。超过 100 个将会分批次同步
     *
     * @param  string $tenantCode   租户号
     * @param  array  $users        需要新增到通行证的用户数据数组
     * @return User[] 创建成功的通行证用户对象数组
     */
    public function batchCreateUsers($tenantCode, array $users)
    {
        if (empty($tenantCode) || empty($users)) {
            return [];
        }

        if (count($users) > 100) {
            $result   = [];
            $usersArr = array_chunk($users, 100);
            foreach ($usersArr as $us) {
                $tmpUsers = $this->internalBatchCreateUsers($tenantCode, $us);
                if (!empty($tmpUsers)) {
                    $result = array_merge($result, $tmpUsers);
                }
            }
            return $result;
        }
        return $this->internalBatchCreateUsers($tenantCode, $users);
    }

    protected function internalBatchCreateUsers($tenantCode, array $users)
    {
        $response = $this->sender->invokeBatchCreateUsersApi($tenantCode, $users);
        if (!$response->isSuccess(true)) {
            return [];
        }

        $resData = $response->toArray();
        if (!empty($resData['faildUsers'])) {
            Log::error('通行证用户同步出现失败数据: ' . json_encode($resData['faildUsers']));
        }

        $successUsers = $resData['data']['successUsers'] ?? [];

        $result = [];
        foreach ($successUsers as $u) {
            $result[] = new User($u['unifiedPassUser']);
        }
        return $result;
    }

    /**
     * 更新通行证用户的资料信息
     *
     * @param  string $unionId      通行证用户 UnionId
     * @param  array  $values       待更新的数据数组
     * @param  string|null $remark  操作备注
     * @return bool
     */
    public function updateUser($unionId, array $values, $remark = null)
    {
        if (empty($user)) {
            return false;
        }

        $response = $this->sender->invokeUpdateUserApi($unionId, $values, $remark);
        return $response->isSuccess(true);
    }

    /**
     * 修改通行证用户登录密码
     *
     * @param  string $unionId      通行证用户 UnionId
     * @param  string $password     新登录密码，可明文可密文
     * @param  bool   $isEncrypted  密码是否密文
     * @param  string $tenantCode   租户号
     * @return bool
     */
    public function changePassword($unionId, $password, $isEncrypted = false, $tenantCode = '')
    {
        if (empty($unionId) || empty($password)) {
            return false;
        }

        $response = $this->sender->invokeChangeUserPasswordApi($unionId, $password, $isEncrypted, $tenantCode);
        return $response->isSuccess(true);
    }

    /**
     * 修改通行证用户手机号
     *
     * @param  string $unionId      通行证用户 UnionId
     * @param  string $mobile       新手机号
     * @param  int    $countryCode  新手机号的区号
     * @param  string|null $remark  操作备注
     */
    public function changeMobile($unionId, $mobile, $countryCode = 86, $remark = null)
    {
        if (empty($unionId) || empty($mobile)) {
            return false;
        }

        $response = $this->sender->invokeChangeUserMobileApi($unionId, $mobile, $countryCode, $remark);
        return $response->isSuccess(true);
    }

    /**
     * 启用通行证用户与某个租户的关联关系
     *
     * @param  string $tenantCode   租户号
     * @param  string $unionId      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return bool
     */
    public function enableUserTenantRelation($tenantCode, $unionId, $remark = null)
    {
        if (empty($tenantCode) || empty($unionId)) {
            return false;
        }

        $response = $this->sender->invokeEnableUserTenantRelationApi($tenantCode, $unionId, $remark);
        return $response->isSuccess(true);
    }

    /**
     * 禁用通行证用户与某个租户的关联关系
     *
     * @param  string $tenantCode   租户号
     * @param  string $unionId      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return bool
     */
    public function disableUserTenantRelation($tenantCode, $unionId, $remark = null)
    {
        if (empty($tenantCode) || empty($unionId)) {
            return false;
        }

        $response = $this->sender->invokeDisableUserTenantRelationApi($tenantCode, $unionId, $remark);
        return $response->isSuccess(true);
    }

    /**
     * 删除通行证用户与某个租户的关联关系
     * 
     * @param  string $tenantCode   租户号
     * @param  string $unionID      通行证用户 UnionId
     * @param  string|null $remark  操作备注
     * @return bool
     */
    public function deleteUserTenantRelation($tenantCode, $unionId, $remark = null)
    {
        if (empty($tenantCode) || empty($unionId)) {
            return false;
        }

        $response = $this->sender->invokeDeleteUserTenantRelationApi($tenantCode, $unionId, $remark);
        return $response->isSuccess(true);
    }

    /**
     * 获取某个通行证用户的所有关联租户信息。请求参数或接口响应异常时，返回空数组
     *
     * @param  string $unionId 通行证用户 UnionId
     * @return array
     */
    public function getUserTenantRelations($unionId)
    {
        if (empty($unionId)) {
            return [];
        }

        $response = $this->sender->invokeUserTenantRelationsApi($unionId);
        if (!$response->isSuccess(true)) {
            return [];
        }

        $resData = $response->toArray();
        return $resData['tenantList'] ?? [];
    }
}
