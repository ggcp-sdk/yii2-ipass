<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 * @since 2021年08月31日15:19:34
 */
namespace iPass\server;

class ServerApis
{
    const GET_TOKEN                    = '/v2/oauth2/auth-token';
    const REFRESH_TOKEN                = '/v2/token/refresh';
    const BATCH_CREATE_USER            = '/server/user/batch-create';
    const BATCH_CREATE_USER_ASYNC      = '/server/user/async-batch-create';
    const CREATE_USERS_ASYNC_PROGRESS  = '/server/user/async-batch-create-progress';
    const USER_LIST_BY_UNIONID         = '/server/user/list-by-union-ids';
    const UPDATE_USER                  = '/server/user/update';
    const CHANGE_PASSWORD              = '/server/user/change-password';
    const CHANGE_MOBILE                = '/server/user/change-mobile';
    const USER_TENANT_RELATIONS        = '/server/user-relation/tenant-relations';
    const ENABLE_USER_TENANT_RELATION  = '/server/user-relation/enable-tenant-relation';
    const DISABLE_USER_TENANT_RELATION = '/server/user-relation/disable-tenant-relation';
    const DELETE_USER_TENANT_RELATION  = '/server/user-relation/delete-tenant-relation';
}
