<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

use RuntimeException;

class ConfigException extends RuntimeException
{
    
}