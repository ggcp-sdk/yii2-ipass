<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

use RuntimeException;

/**
 * 通行证服务端接口异常类
 */
class ServerException extends RuntimeException
{

}
