<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

/**
 * 用户未设置登录密码异常
 */
class NotSetUserPwdException extends BreakStepException
{
    
}