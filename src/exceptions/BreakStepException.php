<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

class BreakStepException extends \RuntimeException
{
    protected $breakData;

    public function __construct($message = '', $code = 0, $breakData = null)
    {
        $this->message = $message;
        $this->code    = $code;

        $this->breakData = $breakData;
    }

    public function getBreakData()
    {
        return $this->breakData;
    }
}
