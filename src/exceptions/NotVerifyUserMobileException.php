<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

/**
 * 用户手机号未验真异常
 */
class NotVerifyUserMobileException extends BreakStepException
{
    
}