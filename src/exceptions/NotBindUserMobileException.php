<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace iPass\exceptions;

/**
 * 用户未绑定手机号异常
 */
class NotBindUserMobileException extends BreakStepException
{
    
}