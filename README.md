统一通行证基础业务服务 SDK

## 配置 & 使用

---

### 按需实例化

> 按需提供配置，实例化对象：

```php
$config = [
    'apiHost' => '统一通行证的接口域名地址',
    'appid'   => '统一通行证应用 AppID',
    'appKey'  => '统一通行证应用 AppKey',
];
$unifiedPass = \iPass\UnifiedPass::instance($config);
...
```

### 服务注册

> 通过框架服务注册的方式提供服务调用。可以在你需要的底层注册处理中完成对应的服务注册，并在后续业务逻辑处理时完成方法调用：

```php
// 服务注册，如需单例使用，可改成 setSingleton()
\Yii::$container->set('iPass', function () {
    $config = [
        'apiHost' => '统一通行证的接口域名地址',
        'appid'   => '统一通行证应用 AppID',
        'appKey'  => '统一通行证应用 AppKey',
    ];
    $unifiedPass = \iPass\UnifiedPass::instance($config);
})

// 服务调用
if (\Yii::$container->has('iPass')) {
    $iPass = \Yii::$container->get('iPass');
    ...
}
```

### 框架组件注册

> 通过框架组件注册的方式提供服务调用。需要你在框架配置文件中加入对应的组件 `components` 配置：

```php
// 组件注册
'ipass' => [
    'class'  => '\iPass\UnifiedPass',
    'config' => [
        'apiHost' => '统一通行证的接口域名地址',
        'appid'   => '统一通行证应用 AppID',
        'appKey'  => '统一通行证应用 AppKey',
    ],
]

// 服务调用
\Yii::$app->iPass->client()->validateAccessToken($accessToken);
...
```

## SDK 功能方法清单

---

### 客户端接口调用示例：

客户端接口的使用场景，用于系统是直接给用户提供服务的时候调用。比如：passport 登录站点希望支持通行证的登录，这时候对于 passport 来说它就是一个给用户提供服务的使用场景。  
客户端接口的使用，通过 UnifiedPass::client() 方法提供，该方法可以返回一个客户端接口的提供者实例。如下代码示例：

```php
$config = [...配置信息];
$client = UnifiedPass::instance($config)->client();

$response = $client->loginByPassword($mobile, $password, $tenantCode)->asLoginResponse();
if (!$response->isSuccess(true)) {
    // 错误处理
    $errmsg = $response->getMessage();
    ...
}
// 登录接口调用成功，执行进一步判断
if (!$response->isLoginSuccess()) {
    // 错误处理
    $errmsg = $response->getMessage();
    ...
}

// 有提供租户号时，登录成功需要拿到授权令牌
$token   = $response->toToken();
$userRes = $client->getUser($token)->asUserResponse();
if ($userRes->isSuccess(true)) {
    // 错误处理
    $errmsg = $userRes->getMessage();
    ...
}

$userInfo = $userRes->toUser();

...
```

### （！即将废弃！！！）通行证用户侧相关方法：

通过 UnifiedPass 实例可以直接调用到的方法

- validateAccessToken 访问令牌校验
- refreshToken 刷新授权令牌
- loginBySmsCode 根据手机号+短信验证码登录
- loginByPwd 根据账号/手机号+密码登录
- loginByTicket 利用临时登录凭证完成登录
- loginByToken 利用生效中的授权令牌换取当前用户在另一个租户下的登录态授权令牌
- register 注册
- getUnifiedPassUser 根据访问令牌获取对应的通行证用户信息
- updateUser 根据登录态更新登录用户自己的基本信息
- changeApproval 变更某个通行证用户的租户认证状态（审批人员审批操作）
- getUserRelateTenantAuths 获取通行证用户有关联的租户认证数据列表
- getResetPasswordTicket 获取重置密码所需的 Ticket
- resetPassword 重置密码
- changePassword 修改密码
- bindMobile 绑定手机号
- sendSmsCode 发送短信验证码
- searchTenant 搜索并获取指定租户的基本信息
- initUnifiedPassUsers （即将废弃，采取新的方式调用）向通行证同步初始化用户数据

### 服务端接口调用相关示例：

服务端接口的使用场景，是在系统后端服务中需要与通行证进行数据交互时调用的接口。比如：第三方系统新增了一个用户，希望同步到通行证中，则可以通过服务端创建通行证用户接口完成数据同步操作。  
服务端接口的使用，通过 UnifiedPass::server() 方法提供，该方法可以返回一个服务端接口的提供者实例。如下代码示例：

```php
$config = [...配置信息];
$server = UnifiedPass::instance($config)->server();

$successUsers = $server->batchCreateUsers($tenantCode, $newUsers);
...
```

### （！！即将废弃！！！）第三方系统后端服务侧相关方法：

服务侧相关方法通过 ManageProvider 服务提供者提供。通过 UnifiedPass 实例下的 getManageProvider 方法或者直接实例化都可以得到一个 ManageProvider 实例

- syncInitUsers 同步阻塞的方式向通行证初始化用户数据（原 UnifiedPass::initUnifiedPassUsers 方法）
- asyncInitUsers 异步的方式向通行证初始化用户数据（待完善）
- getAsyncInitUsersProgress 查 询异步初始化用户数据的进度（待完善）
- enableUserAuth 启用通行证用户与指定租户的关联认证关系
- disableUserAuth 禁用通行证用户与指定租户的关联认证关系
- deleteUserAuth 删除通行证用户与指定租户的关联认证关系
- modifyUser 管理员等非用户自身人员操作修改通行证用户的基本信息
- getUserListByOpenIds （即将废弃）根据指定的 OpenID 清单获取对应的通行证用户信息
- getUserListByUnionIds 根据指定的 UnionID 清单获取某个租户下的通行证用户信息
- getUserListWithPagination （已废弃）通过分页的方式获取当前接入应用有权限的通行证用户信息
