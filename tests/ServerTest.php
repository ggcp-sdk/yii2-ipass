<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 * @since 2021年08月31日17:33:09
 */
namespace iPass\Tests;

use iPass\server\ServerProvider;
use iPass\UnifiedPass;
use PHPUnit\Framework\TestCase;

class ServerTest extends TestCase
{
    private $tenantCode = '';

    private $config = [
        'apiHost' => '',
        'appid'   => '',
        'appKey'  => '',
    ];

    public function testPrepareServer()
    {

        $server = UnifiedPass::instance($this->config)->server();

        $this->assertInstanceOf(ServerProvider::class, $server, '获取通行证服务端访问入口实例失败');

        return $server;
    }

    /**
     * @depends testPrepareServer
     */
    public function testBatchCreateUsers(ServerProvider $server)
    {
        $user = [
            'mobile'   => '',
            'realname' => '',
            'password' => '',
        ];

        $passUsers = $server->batchCreateUsers($this->tenantCode, [$user]);

        $this->assertEquals(1, count($passUsers), '批量创建通行证用户失败');
    }

    private $unionId = '';

    /**
     * @depends testPrepareServer
     */
    public function testChangePassword(ServerProvider $server)
    {
        $password = rand(100000, 999999);
        $changeRes = $server->changePassword($this->unionId, $password);

        $this->assertTrue($changeRes, '修改通行证用户登陆密码失败');
    }

    /**
     * @depends testPrepareServer
     */
    public function testChangeMobile(ServerProvider $server)
    {
        $mobile = 135 . rand(10000000, 99999999);
        $changeRes = $server->changeMobile($this->unionId, $mobile, 86, '单元测试');

        $this->assertTrue($changeRes, '修改通行证用户手机号失败');

        $users = $server->getUsersByUnionIds([$this->unionId], $this->tenantCode);
        
        $this->assertEquals(1, count($users), '根据 UnionId 获取通行证用户，返回数据异常');

        $user = current($users);

        $this->assertEquals($mobile, $user->mobile, '通行证用户手机号修改操作后未能更新为正确手机号');
    }
}
